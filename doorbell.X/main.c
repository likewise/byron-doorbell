/*
 * File:   main.c
 * Author: Hagen Patzke
 *
 * Created on 2020-11-30 23:00
 */


// PIC16F18313 Configuration Bit Settings

// 'C' source line config statements
// Configuration bits: selected in the GUI

// CONFIG1
#pragma config FEXTOSC = OFF    // FEXTOSC External Oscillator mode Selection bits (Oscillator not enabled)
#pragma config RSTOSC = HFINT1  // Power-up default value for COSC bits->HFINTOSC (1MHz)
#pragma config CLKOUTEN = OFF   // Clock Out Enable bit (CLKOUT function is disabled; I/O or oscillator function on OSC2)
#pragma config CSWEN = ON       // Clock Switch Enable bit (Writing to NOSC and NDIV is allowed)
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is enabled)

// CONFIG2
#pragma config MCLRE = ON       // Master Clear Enable bit (MCLR/VPP pin function is MCLR; Weak pull-up enabled )
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#pragma config WDTE = OFF       // Watchdog Timer Enable bits (WDT disabled; SWDTEN is ignored)
#pragma config LPBOREN = OFF    // Low-power BOR enable bit (ULPBOR disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable bits (Brown-out Reset enabled, SBOREN bit ignored)
#pragma config BORV = HIGH      // Brown-out Reset Voltage selection bit (Brown-out voltage (Vbor) set to 2.7V)
#pragma config PPS1WAY = OFF    // PPSLOCK bit One-Way Set Enable bit (The PPSLOCK bit can be set and cleared repeatedly (subject to the unlock sequence))
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable bit (Stack Overflow or Underflow will cause a Reset)
#pragma config DEBUG = OFF      // Debugger enable bit (Background debugger disabled)

// CONFIG3
#pragma config WRT = OFF        // User NVM self-write protection bits (Write protection off)
#pragma config LVP = ON         // Low Voltage Programming Enable bit (Low voltage programming enabled. MCLR/VPP pin function is MCLR. MCLRE configuration bit is ignored.)

// CONFIG4
#pragma config CP = OFF         // User NVM Program Memory Code Protection bit (User NVM code protection disabled)
#pragma config CPD = OFF        // Data NVM Memory Code Protection bit (Data NVM code protection disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>

// Doorbell pattern for the Byron Wireless Doorbell Set from Action
#define BELL_1  0x9d3b85d3UL
#define BELL_2  0x3057d593UL
// Number of packets sent
#define REPEATS 20
// Signal and Gap length for 0-bit
#define BT0_SIG 524
#define BT0_GAP 1544
// Signal and Gap length for 1-bit
#define BT1_SIG 1560
#define BT1_GAP 512
// Signal and Gap length for SYNC
#define SYN_SIG 524
#define SYN_GAP 7244

unsigned long pattern;
char rpt, pos;

// Tell the compiler the working frequency (needed for _delay functions)
#define _XTAL_FREQ 4000000

#define OUT_SIG 0b00100000 // RA5, pin 2
#define OUT_GAP 0b00000000 // all low

void init() {
    LATA    = OUT_GAP;    // all output latches low
    TRISA   = 0b00000000; // RA0/1/2/4/5 output (RA3=MCLR)
    ANSELA  = 0b00000000; // no analog inputs
    WPUA    = 0b00000000; // No weak pull-ups (no input)
    ODCONA  = 0b00000000; // Open Drain Control: all outputs are push-pull
    SLRCONA = 0b00000000; // Slew-rate as fast as possible
    INLVLA  = 0b00000000; // All inputs TTL level
    // CLKRMD CLKR enabled; SYSCMD SYSCLK enabled; FVRMD FVR enabled; IOCMD IOC enabled; NVMMD NVM enabled; 
    PMD0 = 0x00;
    // TMR0MD TMR0 enabled; TMR1MD TMR1 enabled; TMR2MD TMR2 enabled; NCOMD DDS(NCO) enabled; 
    PMD1 = 0x00;
    // DACMD DAC enabled; CMP1MD CMP1 enabled; ADCMD ADC enabled; 
    PMD2 = 0x00;
    // CCP2MD CCP2 enabled; CCP1MD CCP1 enabled; PWM6MD PWM6 enabled; PWM5MD PWM5 enabled; CWG1MD CWG1 enabled; 
    PMD3 = 0x00;
    // MSSP1MD MSSP1 enabled; UART1MD EUSART enabled; 
    PMD4 = 0x00;
    // DSMMD DSM enabled; CLC1MD CLC1 enabled; CLC2MD CLC2 enabled; 
    PMD5 = 0x00;
    // NOSC HFINTOSC, NDIV=1
    OSCCON1 = 0x60;
    // CSWHOLD may proceed; SOSCPWR Low power; SOSCBE crystal oscillator; 
    OSCCON3 = 0x00;
    // LFOEN disabled; ADOEN disabled; SOSCEN disabled; EXTOEN disabled; HFOEN disabled; 
    OSCEN = 0x00;
    // HFFRQ 4_MHz; 
    OSCFRQ = 0x03;
    // HFTUN 0; 
    OSCTUNE = 0x00;
    // WDTPS 1:65536; SWDTEN OFF; 
    WDTCON = 0x16;

}

int main(void) {
    
    
    for (rpt = REPEATS; rpt; --rpt) {

        pattern = BELL_1;

        for (pos = 32; pos; --pos) {
            if (pattern & (1UL << 31)) {
                LATA = OUT_SIG;
                __delay_us(BT1_SIG);
                LATA = OUT_GAP;
                __delay_us(BT1_GAP);
            } else {
                LATA = OUT_SIG;
                __delay_us(BT0_SIG);
                LATA = OUT_GAP;
                __delay_us(BT0_GAP);
            }
            // next bit in pattern
            pattern <<= 1;
        }

        // SYNC BIT
        LATA = OUT_SIG;
        __delay_us(SYN_SIG);
        LATA = OUT_GAP;
        __delay_us(SYN_GAP);
    }

    return 0;
}

