EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Byron Doorbell Transmitter"
Date "2020-12-05"
Rev "1.0"
Comp "Hagen P"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Diode:1N4148 D1
U 1 1 5FCBBABE
P 2250 1950
F 0 "D1" H 2250 2167 50  0000 C CNN
F 1 "1N4148" H 2250 2076 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 2250 1775 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 2250 1950 50  0001 C CNN
	1    2250 1950
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Zener_ALT ZD1
U 1 1 5FCBC5EF
P 2500 2850
F 0 "ZD1" V 2454 2930 50  0000 L CNN
F 1 "D_Zener_5.1V" V 2545 2930 50  0000 L CNN
F 2 "" H 2500 2850 50  0001 C CNN
F 3 "~" H 2500 2850 50  0001 C CNN
	1    2500 2850
	0    1    1    0   
$EndComp
$Comp
L Device:CP C1
U 1 1 5FCBDF80
P 3150 2550
F 0 "C1" H 3268 2596 50  0000 L CNN
F 1 "470uF" H 3268 2505 50  0000 L CNN
F 2 "" H 3188 2400 50  0001 C CNN
F 3 "~" H 3150 2550 50  0001 C CNN
	1    3150 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_EBC Q1
U 1 1 5FCBF5B6
P 2800 2050
F 0 "Q1" V 3128 2050 50  0000 C CNN
F 1 "Q_NPN_EBC" V 3037 2050 50  0000 C CNN
F 2 "" H 3000 2150 50  0001 C CNN
F 3 "~" H 2800 2050 50  0001 C CNN
	1    2800 2050
	0    -1   -1   0   
$EndComp
$Comp
L MCU_Microchip_PIC16:PIC16F18313-IP U1
U 1 1 5FCBFD3F
P 4450 2550
F 0 "U1" H 4450 3331 50  0000 C CNN
F 1 "PIC16F18313-IP" H 4450 3240 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 5050 3200 50  0001 C CNN
F 3 "https://ww1.microchip.com/downloads/en/DeviceDoc/40001799F.pdf" H 4450 2550 50  0001 C CNN
	1    4450 2550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5FCC1652
P 1800 1950
F 0 "J1" H 1718 2167 50  0000 C CNN
F 1 "Power" H 1718 2076 50  0000 C CNN
F 2 "" H 1800 1950 50  0001 C CNN
F 3 "~" H 1800 1950 50  0001 C CNN
	1    1800 1950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2000 1950 2100 1950
Wire Wire Line
	2400 1950 2500 1950
Wire Wire Line
	2500 2100 2500 1950
Connection ~ 2500 1950
Wire Wire Line
	2500 1950 2600 1950
Wire Wire Line
	3000 1950 3150 1950
Wire Wire Line
	3150 1950 3150 2400
Wire Wire Line
	2800 2250 2800 2550
$Comp
L Device:R R1
U 1 1 5FCBD0C9
P 2500 2250
F 0 "R1" H 2570 2296 50  0000 L CNN
F 1 "560R" H 2570 2205 50  0000 L CNN
F 2 "" V 2430 2250 50  0001 C CNN
F 3 "~" H 2500 2250 50  0001 C CNN
	1    2500 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 3150 2500 3150
Connection ~ 2500 3150
Wire Wire Line
	2500 3150 3150 3150
Wire Wire Line
	2500 2550 2500 2700
Wire Wire Line
	2500 2400 2500 2550
Connection ~ 2500 2550
Wire Wire Line
	2800 2550 2500 2550
Wire Wire Line
	5500 2550 5400 2550
Wire Wire Line
	5400 2550 5400 1950
Wire Wire Line
	5400 1950 4450 1950
Connection ~ 4450 1950
Wire Wire Line
	3150 2700 3150 3150
Wire Wire Line
	2000 2050 2000 3150
Wire Wire Line
	2500 3000 2500 3150
Connection ~ 4450 3150
Wire Wire Line
	5300 2450 5300 3150
Wire Wire Line
	5300 3150 4450 3150
Wire Wire Line
	5500 2450 5300 2450
Wire Wire Line
	5050 2650 5500 2650
Wire Wire Line
	3150 1950 4450 1950
Connection ~ 3150 1950
Wire Wire Line
	3150 3150 4450 3150
Connection ~ 3150 3150
Text Notes 2400 3400 0    50   ~ 0
4.5V Power Supply\n
Text Notes 4250 3450 0    50   ~ 0
Byron DBY-23710W\nDoorbell Transmitter Replacement
$Comp
L Connector:Conn_01x03_Female J2
U 1 1 5FCC32A1
P 5700 2550
F 0 "J2" H 5592 2225 50  0000 C CNN
F 1 "FS1000A" H 5592 2316 50  0000 C CNN
F 2 "" H 5700 2550 50  0001 C CNN
F 3 "~" H 5700 2550 50  0001 C CNN
	1    5700 2550
	1    0    0    1   
$EndComp
Text Notes 5500 3150 0    50   ~ 0
TX 433.92MHz
Wire Notes Line
	5700 2100 5700 3000
Wire Notes Line
	5700 3000 5950 3000
Wire Notes Line
	5950 3000 5950 2100
Wire Notes Line
	5950 2100 5700 2100
Text Notes 3100 1850 0    39   ~ 0
Generic NPN Transistor\neg. h_FE=95, V_f=673mV\n
$EndSCHEMATC
